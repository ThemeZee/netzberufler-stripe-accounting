<?php
/*
Plugin Name: Netzberufler Stripe Accounting
Plugin URI: https://netzberufler.com/
Description: Stripe Accounting Tool
Author: ThemeZee
Author URI: https://themezee.com/
Version: 1.0
Text Domain: netzberufler-stripe
Domain Path: /languages/

Netzberufler Stripe Accounting
Copyright(C) 2017, ThemeZee.com - support@themezee.com

*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// Use class to avoid namespace collisions
if ( ! class_exists( 'Netzberufler_Stripe' ) ) :

	/**
	 * Main Netzberufler_Stripe Class
	 *
	 * @package Netzberufler Stripe Accounting
	 */
	class Netzberufler_Stripe {

		/**
		 * Call all Functions to setup the Plugin
		 *
		 * @uses Netzberufler_Stripe::constants() Setup the constants needed
		 * @uses Netzberufler_Stripe::includes() Include the required files
		 * @uses Netzberufler_Stripe::setup_actions() Setup the hooks and actions
		 * @return void
		 */
		static function setup() {

			// Setup Constants
			self::constants();

			// Setup Translation
			add_action( 'plugins_loaded', array( __CLASS__, 'translation' ) );

			// Include Files
			self::includes();

		}

		/**
		 * Setup plugin constants
		 *
		 * @return void
		 */
		static function constants() {

			// Define Version Number
			define( 'NB_STRIPE_VERSION', '1.0' );

			// Plugin Folder Path
			define( 'NB_STRIPE_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );

			// Plugin Folder URL
			define( 'NB_STRIPE_PLUGIN_URL', plugin_dir_url( __FILE__ ) );

			// Plugin Root File
			define( 'NB_STRIPE_PLUGIN_FILE', __FILE__ );

		}

		/**
		 * Load Translation File
		 *
		 * @return void
		 */
		static function translation() {

			load_plugin_textdomain( 'netzberufler-stripe', false, dirname( plugin_basename( NB_STRIPE_PLUGIN_FILE ) ) . '/languages/' );

		}

		/**
		 * Include required files
		 *
		 * @return void
		 */
		static function includes() {

			// Include Admin Setup
			require_once NB_STRIPE_PLUGIN_DIR . 'includes/class-nb-stripe-admin-page.php';
			require_once NB_STRIPE_PLUGIN_DIR . 'includes/class-nb-stripe-table-list.php';

			// Include Functions
			require_once NB_STRIPE_PLUGIN_DIR . 'includes/functions/nb-stripe-notices.php';
			require_once NB_STRIPE_PLUGIN_DIR . 'includes/functions/nb-stripe-helpers.php';

			// Include Tabs Classes
			require_once NB_STRIPE_PLUGIN_DIR . 'includes/pages/class-nb-stripe-entries.php';
			require_once NB_STRIPE_PLUGIN_DIR . 'includes/pages/class-nb-stripe-transactions.php';
			require_once NB_STRIPE_PLUGIN_DIR . 'includes/pages/class-nb-stripe-statistics.php';
			require_once NB_STRIPE_PLUGIN_DIR . 'includes/pages/class-nb-stripe-process.php';
			require_once NB_STRIPE_PLUGIN_DIR . 'includes/pages/class-nb-stripe-process-page.php';
			require_once NB_STRIPE_PLUGIN_DIR . 'includes/pages/class-nb-stripe-import.php';
			require_once NB_STRIPE_PLUGIN_DIR . 'includes/pages/class-nb-stripe-export.php';
		}
	}

	// Run Plugin
	Netzberufler_Stripe::setup();

endif;
