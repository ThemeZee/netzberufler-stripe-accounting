<?php
/***
 * Stripe Entries
 *
 * Displays all stripe entries in a table
 *
 * @package Netzberufler Stripe Accounting
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// Use class to avoid namespace collisions
if ( ! class_exists( 'NB_Stripe_Statistics' ) ) :

	class NB_Stripe_Statistics extends NB_Stripe_Table_List {
		/** Singleton *************************************************************/

		/**
		 * @var instance The one true NB_Stripe_Statistics instance
		 */
		private static $instance;

		/**
		 * @var Database Table
		 */
		private $db_table;

		/**
		 * @var Available Database Fields
		 */
		private $dates;

		/**
		 * @var Current selected fields
		 */
		private $current_date;

		/**
		 * Creates or returns an instance of this class.
		 *
		 * @return NB_Stripe_Statistics A single instance of this class.
		 */
		public static function instance() {

			if ( null == self::$instance ) {
				self::$instance = new self;
			}

			return self::$instance;
		}

		/**
		 * Class Setup
		 *
		 * @return void
		 */
		public function __construct() {
			global $wpdb;

			// Set Tables
			$this->db_table = $wpdb->prefix . 'nb_stripe_statistics';
			$entries_table = $wpdb->prefix . 'nb_stripe_entries';

			// Set Database Fields Variables
			$this->dates = $this->get_monthly_dates( $entries_table, 'date' );

			// Set Current Variables
			$this->current_date = ( isset( $_GET['date'] ) and $_GET['date'] > 0 ) ? (int) $_GET['date'] : $this->dates[0]->year . $this->dates[0]->month;

		}

		/**
		 * Display Statistics Table
		 *
		 * @return void
		 */
		function statistics_table() {
			global $wpdb;

			// Set Year and Month
			$year = substr( $this->current_date, 0, 4 );
			$month = substr( $this->current_date, 4, 2 );

			// Process Stats
			if ( ! empty( $_POST ) && check_admin_referer( 'netzberufler_stripe_stats', 'netzberufler_stripe_stats_nonce' ) ) {

				$this->process_stripe_stats( $year, $month );

			}

			// Get all transactions
			$transactions = $wpdb->get_results( $wpdb->prepare(
				"SELECT * FROM $this->db_table
				WHERE year = %d AND month = %d ORDER BY year DESC, month DESC", $year, $month ) );

			// Display Header
			echo '<h1>' . esc_html__( 'Stripe Statistics', 'netzberufler-stripe' ) . '</h1>';

			// Display Filter Navigation
			$this->table_navigation();
			?>

			<table class="statistics-table wp-list-table widefat fixed posts" cellspacing="0">

			<thead>

				<tr>
					<th style="width: 50%"><?php esc_html_e( 'Type', 'netzberufler-stripe' ); ?></th>
					<th style="width: 50%"><?php esc_html_e( 'Amount', 'netzberufler-stripe' ); ?></th>
				</tr>

			</thead>

			<tbody>

				<tr>

					<td>

						<table class="widefat" cellspacing="0">

							<tr><td><strong><?php esc_html_e( 'Verfügbares Guthaben (alt)', 'netzberufler-stripe' ); ?></strong></td></tr>
							<tr class="alternate"><td><?php esc_html_e( 'Erhaltene Zahlungen', 'netzberufler-stripe' ); ?></td></tr>
							<tr><td><?php esc_html_e( 'Rückzahlungen', 'netzberufler-stripe' ); ?></td></tr>
							<tr class="alternate"><td><?php esc_html_e( 'Gebühren', 'netzberufler-stripe' ); ?></td></tr>
							<tr><td><?php esc_html_e( 'Abbuchungen', 'netzberufler-stripe' ); ?></td></tr>
							<tr class="alternate"><td><?php esc_html_e( 'Nicht zugeordnet', 'netzberufler-stripe' ); ?></td></tr>
							<tr><td class="total"><?php esc_html_e( 'Verfügbares Guthaben (neu)', 'netzberufler-stripe' ); ?></td></tr>

						</table>

					</td>


				<?php
				// Display Transactions
				$i = 0;
				foreach ( $transactions as $transaction ) :
					$i++;
					$class = ( $i % 2 != 0 ) ? 'alternate ' : '';
					?>

					<td>

						<table class="widefat" cellspacing="0">

							<tr><td><strong><?php echo number_format( $transaction->balance_old, 2, ',', '.' ); ?></strong></td></tr>
							<tr class="alternate"><td><?php echo number_format( $transaction->charges, 2, ',', '.' ); ?></td></tr>
							<tr><td><?php echo number_format( $transaction->refunds, 2, ',', '.' ); ?></td></tr>
							<tr class="alternate"><td><?php echo number_format( $transaction->fees, 2, ',', '.' ); ?></td></tr>
							<tr><td><?php echo number_format( $transaction->payouts, 2, ',', '.' ); ?></td></tr>
							<tr class="alternate"><td><?php echo number_format( $transaction->rest, 2, ',', '.' ); ?></td></tr>
							<tr><td class="total"><?php echo number_format( $transaction->balance, 2, ',', '.' ); ?></td></tr>

						</table>

					</td>

			<?php endforeach; ?>

					</tr>

				</tbody>

			</table>

			<?php

			// Display Filter Navigation
			$this->table_navigation();

		}

		private function table_navigation() {
			?>

			<div class="tablenav top">

				<div class="alignleft actions">

					<form class="stats-navigation" action="<?php echo admin_url( 'admin.php' ); ?>" method="get">
						<input type="hidden" name="page" value="netzberufler-stripe" />
						<input type="hidden" name="tab" value="statistics" />

							<?php $this->monthly_dates_select( $this->dates, $this->current_date ); ?>

							<input name="Submit" class="button-secondary" type="submit" value="<?php esc_html_e( 'Filter', 'netzberufler-stripe' ); ?>" />

					</form>

				</div>

				<div class="alignright actions">

					<form action="<?php echo admin_url( 'admin.php?page=netzberufler-stripe&tab=statistics&date=' . $this->current_date ); ?>" method="post">

						<?php wp_nonce_field( 'netzberufler_stripe_stats', 'netzberufler_stripe_stats_nonce' ); ?>

						<input name="Submit" class="button-secondary" type="submit" value="<?php esc_html_e( 'Calculate Stats', 'netzberufler-stripe' ); ?>" />

					</form>

				</div>

			</div>

			<?php
		}

		function monthly_dates_select( $dates, $current_date ) {
			?>

				<select id="date" name="date">

				<?php
				foreach ( $dates as $date ) :

					$key = $date->year . $date->month;
					$value = date( 'F Y', mktime( 0, 0, 0, $date->month, 1, $date->year ) );

					echo '<option value="' . esc_attr( $key ) . '"' . selected( $key, $current_date, false ) . '>' . esc_html( $value ) . '</option>';

					endforeach;
				?>

				</select>

			<?php
		}

		/**
		 * Process Stripe Stats
		 *
		 * @param int $invoice_id
		 */
		function process_stripe_stats( $year = 0, $month = 0 ) {
			global $wpdb;

			// Set Variables
			$balance_old = 0;
			$charges = 0;
			$refunds = 0;
			$fees = 0;
			$payouts = 0;
			$rest = 0;

			// Set Tables
			$entries_table = $wpdb->prefix . 'nb_stripe_entries';
			$statistic_table = $wpdb->prefix . 'nb_stripe_statistics';

			// Get all transactions for month
			$transactions = $wpdb->get_results( $wpdb->prepare(
				"SELECT * FROM $entries_table
				WHERE YEAR(date) = %d AND MONTH(date) = %d
				ORDER BY date DESC, time DESC", $year, $month ) );

			// Loop transactions
			foreach ( $transactions as $transaction ) :

				if ( 'charge' == $transaction->type ) {

					$charges += $transaction->total;

				} elseif ( 'refund' == $transaction->type ) {

					$refunds += $transaction->total;

				} elseif ( 'fees' == $transaction->type ) {

					$fees += $transaction->total;

				} elseif ( 'payout' == $transaction->type ) {

					$payouts += $transaction->total;

				} else {

					$rest += $transaction->total;

				}

			endforeach;

			// Set previous month and year
			$now = mktime( 0, 0, 0, $month, 15, $year );
			$previous_year = date( 'Y', strtotime( 'last day of last month', $now ) );
			$previous_month = date( 'm', strtotime( 'last day of last month', $now ) );

			// Get old Balance
			$query = $wpdb->get_row( $wpdb->prepare( "SELECT * FROM $statistic_table WHERE year = %d AND month = %d", $previous_year, $previous_month ) );
			$balance_old = isset( $query->balance ) ? (float) $query->balance : 0;

			// Calculate new Balance
			$balance = $balance_old + $charges + $refunds + $fees + $payouts + $rest;

			// Enter Stats
			$data = array(
				'year'        => $year,
				'month'       => $month,
				'currency'    => 'EUR',
				'balance_old' => $balance_old,
				'charges'     => $charges,
				'refunds'     => $refunds,
				'fees'        => $fees,
				'payouts'     => $payouts,
				'rest'        => $rest,
				'balance'     => $balance,
			);

			// Insert on duplicate
			NB_Stripe_Process::insert_on_duplicate( $statistic_table, $data );

		}
	}

endif;
