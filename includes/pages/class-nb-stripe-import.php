<?php
/***
 * NB Stripe Import
 *
 * Displays all invoices in a table
 *
 * @package Netzberufler Stripe Accounting
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}


// Use class to avoid namespace collisions
if ( ! class_exists( 'NB_Stripe_Import' ) ) :

	class NB_Stripe_Import {

		// add Stripe import page
		static function import( $input = '' ) {

			// Set Variables
			global $wpdb;
			$stripe_table = $wpdb->prefix . 'nb_stripe_transactions';

			// Check if input exists
			if ( ! isset( $input ) or '' == $input ) {
				nb_stripe_display_error_notice( esc_html__( 'Error: No form data sent.', 'netzberufler-stripe' ) );
				return;
			}

			// If the "CSV File URL" input field is empty
			if ( empty( $_POST['csv_file'] ) ) {
				nb_stripe_display_error_notice( esc_html__( 'Error: No file was selected. Please enter a File URL.', 'netzberufler-stripe' ) );
				return;
			}

			// Check that "CSV File URL" has proper .csv file extension
			$ext = pathinfo( $_POST['csv_file'], PATHINFO_EXTENSION );
			if ( 'csv' !== $ext ) {
				nb_stripe_display_error_notice( esc_html__( 'Error: The input file does not contain the .csv file extension. Please choose a valid .csv file.', 'netzberufler-stripe' ) );
				return;
			}

			// Open the .csv file and get it's contents
			if ( ( $fh = @fopen( $_POST['csv_file'], 'r' ) ) !== false ) {

				// Set variables
				$values = array();

				// Read CSV File and built $values array
				while ( ( $row = fgetcsv( $fh ) ) !== false ) {  // Get file contents and set up row array

					$row = self::format_csv( $row ); // Format CSV Values

					// Only import the data we need
					$data = array(
						'id'          => $row[0],
						'date'        => $row[8],
						'type'        => $row[1],
						'payment_id'  => $row[2],
						'total'       => $row[3],
						'fee'         => $row[4],
						'subtotal'    => $row[6],
						'currency'    => $row[7],
						'description' => $row[10],
						'email'       => $row[16],
					);

					// Create values string line for database input.
					$values[] = '("' . implode( '", "', $data ) . '")';  // Each new line of .csv file becomes an array
				}

				// If there are rows in the .csv file, continue
				if ( ! empty( $values ) && ( count( $values ) > 1 ) ) {

					// Remove first Description row of Stripe Import CSV
					$values = array_slice( $values, 1 );

					// Get database columns.
					$db_cols = array_keys( $data );

					// Format $db_cols to a string
					$db_cols_implode = implode( ',', $db_cols );

					// Format $values to a string
					$values_implode = implode( ',', $values );

					// Setup sql 'on duplicate update' loop
					$update_on_duplicate = ' ON DUPLICATE KEY UPDATE ';
					foreach ( $db_cols as $db_col ) {
						$update_on_duplicate .= "$db_col=VALUES($db_col),";
					}
					$update_on_duplicate = rtrim( $update_on_duplicate, ',' );

					// INSERT INTO DATABASE
					$sql = 'INSERT INTO ' . $stripe_table . ' (' . $db_cols_implode . ') ' . 'VALUES ' . $values_implode . $update_on_duplicate;
					$db_query_update = $wpdb->query( $sql );

					// Add Messages
					if ( $db_query_update ) {

						nb_stripe_display_update_notice( esc_html__( 'All Stripe transactions have been successfully imported.', 'netzberufler-stripe' ) );
						return;

					} elseif ( ( 0 === $db_query_update ) ) {

						nb_stripe_display_update_notice( esc_html__( 'There were no rows to update. All values already exist in the database.', 'netzberufler-stripe' ) );
						return;

					} else {

						nb_stripe_display_error_notice( esc_html__( 'Error: Database Import failed.', 'netzberufler-stripe' ) );
						return;

					}
				} else {

					nb_stripe_display_error_notice( esc_html__( 'Error: No values found.', 'netzberufler-stripe' ) );
					return;

				} // End if().
			} else {

				nb_stripe_display_error_notice( esc_html__( 'Error: No valid .csv file was found. Please check the File URL and ensure it points to a valid .csv file.', 'netzberufler-stripe' ) );
				return;

			} // End if().
		}

		/**
	 * Display Stripe CSV Import Form
	 *
	 * @return void
	*/
		static function import_form() {

			// Check if Import Form is sent
			if ( ! empty( $_POST ) && check_admin_referer( 'netzberufler_stripe_import', 'netzberufler_stripe_import_nonce' ) ) {

				// Import CSV
				self::import( $_POST );

			}
			?>

			<h1><?php esc_html_e( 'Import Stripe CSV', 'netzberufler-stripe' ); ?></h1>

		<form action="<?php echo admin_url( 'admin.php?page=netzberufler-stripe&tab=import' ); ?>" method="post">

			<table class="form-table">

				<tr>

					<th scope="row"><?php esc_html_e( 'Upload CSV', 'netzberufler-stripe' ); ?></th>

					<td>

						<input type="text" id="csv_file" name="csv_file" size="70" value="" />
						<span>
							<input type="button" class="csv_file_button button-secondary" value="<?php esc_html_e( 'Upload Stripe CSV', 'netzberufler-stripe' ); ?>"/>
						</span>

					</td>

				</tr>

			</table>

			<?php wp_nonce_field( 'netzberufler_stripe_import', 'netzberufler_stripe_import_nonce' ); ?>

			<p><input name="Submit" class="button-primary" type="submit" value="<?php esc_html_e( 'Import Stripe CSV', 'netzberufler-stripe' ); ?>" /></p>

		</form>

		<?php

		}


		// Format CSV Row
		static function format_csv( $row ) {

			// Format Numbers - Replace commas with dots
			$row[3] = str_replace( ',', '.', $row[3] ); // total
			$row[4] = str_replace( ',', '.', $row[4] ); // fee
			$row[6] = str_replace( ',', '.', $row[6] ); // subtotal

			// Format Currency
			$row[7] = strtoupper( $row[7] );

			return $row;
		}

	}

endif;
