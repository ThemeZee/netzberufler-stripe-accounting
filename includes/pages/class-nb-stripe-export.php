<?php
/***
 * NB Stripe Import
 *
 * Displays all invoices in a table
 *
 * @package Netzberufler Stripe Accounting
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// Use class to avoid namespace collisions
if ( ! class_exists( 'NB_Stripe_Export' ) ) :

	class NB_Stripe_Export {

		// Display Export Page
		static function export() {
			global $wpdb;

			// Set Table
			$db_table = $wpdb->prefix . 'nb_stripe_entries';

			// Check if Export Form is sent
			if ( ! empty( $_POST ) && check_admin_referer( 'netzberufler_stripe_export', 'netzberufler_stripe_export_nonce' ) ) {

				if ( isset( $_POST['date'] ) and $_POST['date'] > 0 ) {

					// Set Year and Month
					$year = substr( $_POST['date'], 0, 4 );
					$month = substr( $_POST['date'], 4, 2 );

					// Export CSV.
					$message = self::export_csv( $year, $month );

					// Display Notice
					nb_stripe_display_update_notice( $message );

				}
			}

			// Get Available Dates
			$dates = NB_Stripe_Table_List::get_monthly_dates( $db_table, 'date' );
			?>

			<h1><?php esc_html_e( 'Export Stripe Entries', 'netzberufler-stripe' ); ?></h1>

			<form action="<?php echo admin_url( 'admin.php?page=netzberufler-stripe&tab=export' ); ?>" method="post">

				<table class="form-table">

					<tr>

						<th scope="row"><?php esc_html_e( 'Select Export Period', 'netzberufler-stripe' ); ?></th>

						<td>

							<select id="date" name="date">

							<?php
							foreach ( $dates as $date ) :

								$key = $date->year . $date->month;
								$value = date( 'F Y', mktime( 0, 0, 0, $date->month, 1, $date->year ) );

								echo '<option value="' . esc_attr( $key ) . '">' . esc_html( $value ) . '</option>';

								endforeach;
							?>

							</select>

							</td>

						</tr>

					</table>

					<?php wp_nonce_field( 'netzberufler_stripe_export', 'netzberufler_stripe_export_nonce' ); ?>

					<p><input name="Submit" class="button-primary" type="submit" value="<?php esc_html_e( 'Export', 'netzberufler-stripe' ); ?>" /></p>

				</form>

			<?php
		}

		// Export CSV
		static function export_csv( $year = 0, $month = 0 ) {
			global $wpdb;

			// Return early if year or month missing
			if ( $year < 1 or $month < 1 ) {
				return esc_html__( 'Please select an export period.', 'netzberufler-stripe' );
			}

			// Set Table
			$db_table = $wpdb->prefix . 'nb_stripe_entries';

			// SQL Query
			$db_query = $wpdb->prepare( "SELECT date, description, email, payment_id, total, currency, invoice_id, konto, gegenkonto, kennzeichen, kurs
			FROM $db_table WHERE year(date) = %d AND month(date) = %d ORDER BY date DESC, time DESC", $year, $month );

			// Get Entries from DB
			$entries = $wpdb->get_results( $db_query );

			// Write CSV File
			if ( $entries ) {

				// Set File Path
				$file_path = WP_CONTENT_DIR . '/export/stripe/' . $year . '/' . sprintf( '%02d', $month ) . '/';

				// Create Folder
				wp_mkdir_p( $file_path );

				// Set file name
				$file_name = 'stripe_payments_' . $year . '_' . $month . '.csv';

				// Create File
				$file_path = fopen( $file_path . $file_name, 'w' );

				// Write Header Line
				$header_line = utf8_decode( 'Datum, Buchungstext, ZusatzinfoArt, Transaktionsnummer, Betrag, WKZ, Rechnungsnummer, Konto, Gegenkonto, Soll/Haben, Kurs' ) . "\r\n";
				fputs( $file_path, $header_line );

				// Loop through entries
				foreach ( $entries as $entry ) {

					// Do not export entries without Gegenkonto.
					if ( $entry->gegenkonto > 0 ) {

						// Set Date
						$entry_date = date( 'd.m.Y', strtotime( $entry->date ) );

						// Set Buchungstext.
						$buchungstext = ( '' !== $entry->email ) ? $entry->description . ', ' . $entry->email : $entry->description;

						// Entry Array
						$entry_line = array(
						self::string_value( $entry_date ),
						self::string_value( $buchungstext ),
						self::string_value( 'TransNr' ),
						self::string_value( $entry->payment_id ),
						self::price_value( $entry->total ),
						self::string_value( $entry->currency ),
						self::string_value( $entry->invoice_id ),
						self::integer_value( $entry->konto ),
						self::integer_value( $entry->gegenkonto ),
						self::string_value( $entry->kennzeichen ),
						self::kurs_value( $entry->kurs ),
						);
						$entry_fields = implode( $entry_line, ';' );

						// Encode to Ansi
						$entry_fields = utf8_decode( $entry_fields ) . "\r\n";

						// Write Line
						fputs( $file_path, $entry_fields );

					}
				}

				fclose( $file_path );

				// Export Period
				$period = date( 'F Y', mktime( 0, 0, 0, $month, 1, $year ) );

				// Return Completed Message
				return sprintf( __( 'CSV File for %1$s has been successfully generated', 'netzberufler-stripe' ), $period );

			} else {

				return esc_html__( 'No Stripe entries found.', 'netzberufler-stripe' );

			} // End if().
		}

		// String Value
		static function string_value( $string ) {

			// Escape Double Quotes
			$string = str_replace( '"', '""', $string );
			return '"' . $string . '"';
		}

		// Integer Value
		static function integer_value( $integer ) {

			// Remove 0
			$integer = intval( abs( $integer ) );
			$integer = ( $integer > 0 ) ? $integer : '';

			return $integer;
		}

		/**
		 * Return Price with Comma as Decimal
		 *
		 * @return string Formatted Price
		*/
		static function price_value( $price = 0 ) {

			// Format Price
			$price = floatval( abs( round( $price, 2 ) ) );
			$price = number_format( $price, 2, ',', '' );

			return $price;

		}

		/**
		 * Return Kurs with Comma as Decimal
		 *
		 * @return string Formatted Kurs
		*/
		static function kurs_value( $kurs = 0 ) {

			if ( $kurs <= 0 ) {
				return '';
			}

			// Format Kurs
			$kurs = floatval( round( $kurs, 6 ) );
			$kurs = number_format( $kurs, 6, ',', '' );

			return $kurs;
		}
	}
endif;
