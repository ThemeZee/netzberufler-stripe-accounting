<?php
/***
 * NB Stripe Process
 *
 * Displays all invoices in a table
 *
 * @package Netzberufler Stripe Accounting
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// Use class to avoid namespace collisions
if ( ! class_exists( 'NB_Stripe_Process_Page' ) ) :

	class NB_Stripe_Process_Page {

		/**
		 * Display Process Form
		 *
		 * @return void
		 */
		static function process_form() {
			global $wpdb;

			// Check if Import Form is sent
			if ( ! empty( $_POST ) && check_admin_referer( 'netzberufler_stripe_process', 'netzberufler_stripe_process_nonce' ) ) {

				if ( isset( $_POST['date'] ) and $_POST['date'] > 0 ) {

					// Set Year and Month
					$year = substr( $_POST['date'], 0, 4 );
					$month = substr( $_POST['date'], 4, 2 );

					// Redirect to Process Page
					$redirect = add_query_arg( array(
						'page'   => 'netzberufler-stripe',
						'tab'    => 'process',
						'year'   => (int) $year,
						'month'  => (int) $month,
					), admin_url( 'admin.php' ) );
					?>

					<script type="text/javascript">
					document.location.href = "<?php echo $redirect; ?>";
				</script>

				<?php
				} else {

					nb_stripe_display_error_notice( esc_html__( 'Error: Please select a process period.', 'netzberufler-stripe' ) );

				}
			}

			// Set Table
			$db_table = $wpdb->prefix . 'nb_stripe_transactions';

			// Get Available Dates
			$dates = NB_Stripe_Table_List::get_monthly_dates( $db_table, 'date' );
			?>

			<h1><?php esc_html_e( 'Process Stripe Transactions', 'netzberufler-stripe' ); ?></h1>

			<form action="<?php echo admin_url( 'admin.php?page=netzberufler-stripe&tab=process-page' ); ?>" method="post">

				<table class="form-table">

					<tr>

						<th scope="row"><?php esc_html_e( 'Select Process Period', 'netzberufler-stripe' ); ?></th>

						<td>

							<select id="date" name="date">

								<?php
								foreach ( $dates as $date ) :

									$key = $date->year . $date->month;
									$value = date( 'F Y', mktime( 0, 0, 0, $date->month, 1, $date->year ) );

									echo '<option value="' . esc_attr( $key ) . '">' . esc_html( $value ) . '</option>';

								endforeach;
								?>

							</select>

						</td>

					</tr>

				</table>

				<?php wp_nonce_field( 'netzberufler_stripe_process', 'netzberufler_stripe_process_nonce' ); ?>

				<p><input name="Submit" class="button-primary" type="submit" value="<?php esc_html_e( 'Start Process', 'netzberufler-stripe' ); ?>" /></p>

			</form>

			<?php
		}
	}

endif;
