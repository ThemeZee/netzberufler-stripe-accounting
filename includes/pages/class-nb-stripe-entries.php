<?php
/***
 * Stripe Entries
 *
 * Displays all stripe entries in a table
 *
 * @package Netzberufler Stripe Accounting
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// Use class to avoid namespace collisions.
if ( ! class_exists( 'NB_Stripe_Entries' ) ) :

	class NB_Stripe_Entries extends NB_Stripe_Table_List {
		/** Singleton *************************************************************/

		/**
		 * @var instance The one true NB_Stripe_Entries instance
		 */
		private static $instance;

		/**
		 * @var Database Table
		 */
		private $db_table;

		/**
		 * @var Available Database Fields
		 */
		private $dates;
		private $types;
		private $currencies;

		/**
		 * @var Current selected fields
		 */
		private $current_date;
		private $current_type;
		private $current_currency;

		/**
		 * Creates or returns an instance of this class.
		 *
		 * @return NB_Stripe_Entries A single instance of this class.
		 */
		public static function instance() {

			if ( null == self::$instance ) {
				self::$instance = new self;
			}

			return self::$instance;
		}

		/**
		 * Class Setup
		 *
		 * @return void
		 */
		public function __construct() {
			global $wpdb;

			// Set DB Table
			$this->db_table = $wpdb->prefix . 'nb_stripe_entries';

			// Set Database Fields Variables
			$this->dates = $this->get_monthly_dates( $this->db_table, 'date' );
			$this->types = $this->get_filter_fields( $this->db_table, 'type' );
			$this->currencies = $this->get_filter_fields( $this->db_table, 'currency' );

			// Set Current Variables
			$this->current_date = ( isset( $_GET['date'] ) and $_GET['date'] > 0 ) ? (int) $_GET['date'] : '';
			$this->current_type = ( isset( $_GET['type'] ) and '' !== $_GET['type'] ) ? esc_attr( $_GET['type'] ) : '';
			$this->current_currency = ( isset( $_GET['currency'] ) and '' !== $_GET['currency'] ) ? esc_attr( $_GET['currency'] ) : '';
		}

		/**
		 * Display Entries Table
		 *
		 * @return void
		 */
		function entries_table() {
			global $wpdb;

			// Set Year and Month
			$year = substr( $this->current_date, 0, 4 );
			$month = substr( $this->current_date, 4, 2 );

			// Set DB Query
			$db_query = "SELECT * FROM $this->db_table WHERE 1=1";
			$db_query .= ( '' !== $this->current_date ) ? $wpdb->prepare( ' AND year(date) = %d AND month(date) = %d', $year, $month ) : '';
			$db_query .= ( '' !== $this->current_type ) ? " AND type = '" . $this->current_type . "'" : '';
			$db_query .= ( '' !== $this->current_currency ) ? " AND currency = '" . $this->current_currency . "'" : '';

			// Pagination
			$this->setup_pagination( $db_query );
			$db_query .= $wpdb->prepare( ' ORDER BY date DESC, time DESC LIMIT %d, %d', $this->offset , $this->items_per_page );

			// Get all entries
			$transactions = $wpdb->get_results( $db_query );

			// Display Header
			echo '<h1>' . esc_html__( 'Stripe Entries', 'netzberufler-stripe' ) . '</h1>';

			// Display Filter Navigation
			$this->table_navigation();
			?>

			<table class="wp-list-table widefat fixed posts" cellspacing="0">

			<thead>

				<tr>
					<th style="width: 7%"><?php esc_html_e( 'Date', 'netzberufler-stripe' ); ?></th>
					<th style="width: 7%"><?php esc_html_e( 'Type', 'netzberufler-stripe' ); ?></th>
					<th style="width: 24%"><?php esc_html_e( 'Description', 'netzberufler-stripe' ); ?></th>
					<th style="width: 17%"><?php esc_html_e( 'Payment ID', 'netzberufler-stripe' ); ?></th>
					<th style="width: 7%"><?php esc_html_e( 'Total', 'netzberufler-stripe' ); ?></th>
					<th style="width: 7%"><?php esc_html_e( 'Currency', 'netzberufler-stripe' ); ?></th>
					<th style="width: 7%"><?php esc_html_e( 'Invoice', 'netzberufler-stripe' ); ?></th>
					<th style="width: 7%"><?php esc_html_e( 'Customer', 'netzberufler-stripe' ); ?></th>
					<th style="width: 7%"><?php esc_html_e( 'Konto', 'netzberufler-stripe' ); ?></th>
					<th style="width: 7%"><?php esc_html_e( 'Gegenkonto', 'netzberufler-stripe' ); ?></th>
					<th style="width: 3%"><?php esc_html_e( 'S/H', 'netzberufler-stripe' ); ?></th>
				</tr>

			</thead>

			<?php
			// Display Transactions
			$i = 0;
			foreach ( $transactions as $transaction ) :
				$i++;
				$class = ( 0 !== $i % 2 ) ? 'alternate ' : '';

				$description = ( '' !== $transaction->email ) ? $transaction->description . ', ' . $transaction->email : $transaction->description;
				?>

				<tr class="<?php echo $class; ?>">

					<td><?php echo $transaction->date; ?></td>
					<td><?php echo $transaction->type; ?></td>
					<td><?php echo $description; ?></td>
					<td><?php echo $transaction->payment_id; ?></td>
					<td><?php echo nb_stripe_format_price( $transaction->total, $transaction->currency ); ?></td>
					<td><?php echo $transaction->currency; ?></td>
					<td><?php echo $transaction->invoice_id; ?></td>
					<td><?php echo $transaction->customer_id; ?></td>
					<td><?php echo $transaction->konto; ?></td>
					<td><?php echo $transaction->gegenkonto; ?></td>
					<td><?php echo $transaction->kennzeichen; ?></td>

				</tr>

			<?php endforeach; ?>

				<tfoot>

				<tr>
					<th style="width: 10%"><?php esc_html_e( 'Date', 'netzberufler-stripe' ); ?></th>
					<th style="width: 15%"><?php esc_html_e( 'Type', 'netzberufler-stripe' ); ?></th>
					<th style="width: 15%"><?php esc_html_e( 'Description', 'netzberufler-stripe' ); ?></th>
					<th style="width: 15%"><?php esc_html_e( 'Payment ID', 'netzberufler-stripe' ); ?></th>
					<th style="width: 7%"><?php esc_html_e( 'Total', 'netzberufler-stripe' ); ?></th>
					<th style="width: 7%"><?php esc_html_e( 'Currency', 'netzberufler-stripe' ); ?></th>
					<th style="width: 7%"><?php esc_html_e( 'Invoice', 'netzberufler-stripe' ); ?></th>
					<th style="width: 7%"><?php esc_html_e( 'Customer', 'netzberufler-stripe' ); ?></th>
					<th style="width: 7%"><?php esc_html_e( 'Konto', 'netzberufler-stripe' ); ?></th>
					<th style="width: 7%"><?php esc_html_e( 'Gegenkonto', 'netzberufler-stripe' ); ?></th>
					<th style="width: 3%"><?php esc_html_e( 'S/H', 'netzberufler-stripe' ); ?></th>
				</tr>

				</tfoot>

			</table>

			<?php
			// Display Filter Navigation
			$this->table_navigation();
		}

		private function table_navigation() {
			?>

			<div class="tablenav top">

				<form class="stats-navigation" action="<?php echo admin_url( 'admin.php' ); ?>" method="get">
					<input type="hidden" name="page" value="netzberufler-stripe" />
					<input type="hidden" name="tab" value="entries" />

					<div class="alignleft actions">

						<?php $this->monthly_dates_select( $this->dates, $this->current_date ); ?>
						<?php $this->type_select( $this->types, $this->current_type ); ?>
						<?php $this->currency_select( $this->currencies, $this->current_currency ); ?>

						<input name="Submit" class="button-secondary" type="submit" value="<?php esc_html_e( 'Filter', 'netzberufler-stripe' ); ?>" />

					</div>

					<div class="alignright tablenav-pages">

						<?php $this->pagination( $this->total_items, $this->total_pages, $this->current_page ); ?>

					</div>

				</form>

			</div>

			<?php
		}

		function type_select( $available_types, $current_type ) {
			?>

			<select id="type" name="type">

				<?php
				echo '<option value="" ' . selected( '', $current_type, false ) . '>' . esc_html__( 'All Types', 'netzberufler-stripe' ) . '</option>';

				foreach ( $available_types as $value ) :

					echo '<option value="' . esc_attr( $value->type ) . '"' . selected( $value->type, $current_type, false ) . '>' . esc_html( $value->type ) . '</option>';

					endforeach;
				?>

			</select>

			<?php
		}

		function currency_select( $currencies, $current_currency ) {
			?>

			<select id="currency" name="currency">

				<?php
				echo '<option value="" ' . selected( '', $current_currency, false ) . '>' . esc_html__( 'All Currencies', 'netzberufler-stripe' ) . '</option>';

				foreach ( $currencies as $value ) :

					echo '<option value="' . esc_attr( $value->currency ) . '"' . selected( $value->currency, $current_currency, false ) . '>' . esc_html( $value->currency ) . '</option>';

					endforeach;
				?>

			</select>

			<?php
		}
	}

endif;
