<?php
/***
 * NB Stripe Process
 *
 * Displays all invoices in a table
 *
 * @package Netzberufler Stripe Accounting
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// Use class to avoid namespace collisions
if ( ! class_exists( 'NB_Stripe_Process' ) ) :

	class NB_Stripe_Process {

		/**
		 * Start the batch processor
		 *
		 * @access  public
		 * @since   1.0
		*/
		public static function process() {

			// Return early when year or month missing
			if ( ! ( isset( $_GET['year'] ) or isset( $_GET['month'] ) ) ) {
				return;
			}

			// Set Variables
			$year 		= absint( $_GET['year'] );
			$month		= absint( $_GET['month'] );
			$step		= isset( $_GET['step'] )	? absint( $_GET['step'] )	: 1;
			$total		= isset( $_GET['total'] )	? absint( $_GET['total'] )	: false;
			$processed	= round( ( $step * 50 - 50 ), 0 );
			$processed = ( $processed > $total ) ? $total : $processed;

			// Redirect URL
			$redirect = add_query_arg( array(
				'action'	=> 'nb_stripe_process',
				'year'		=> $year,
				'month'		=> $month,
				'step'		=> $step,
				'total'		=> $total,
				'_wpnonce'	=> wp_create_nonce( 'nb-stripe-nonce' ),
			), admin_url( 'admin.php' ) );
			?>

			<h2><?php esc_html_e( 'Process Stripe Transactions', 'netzberufler-stripe' ); ?></h2>

			<div id="nb-stripe-processing">

				<?php if ( ! empty( $total ) ) : ?>

					<p><strong><?php printf( esc_html__( '%1$d transactions of %2$d processed', 'netzberufler-stripe' ), $processed, $total ); ?></strong></p>

				<?php endif; ?>

			</div>

			<script type="text/javascript">
				document.location.href = "<?php echo $redirect; ?>";
			</script>

			<?php
		}

		public static function process_batch() {
			global $wpdb;

			if ( empty( $_REQUEST['action'] ) || 'nb_stripe_process' != $_REQUEST['action'] ) {
				return;
			}

			if ( ! wp_verify_nonce( $_GET['_wpnonce'], 'nb-stripe-nonce' ) ) {
				return;
			}

			ignore_user_abort( true );

			if ( ! ini_get( 'safe_mode' ) ) {
				@set_time_limit( 0 );
			}

			$year  = isset( $_GET['year'] )  ? absint( $_GET['year'] )  : 1;
			$month  = isset( $_GET['month'] )  ? absint( $_GET['month'] )  : 1;
			$step  = isset( $_GET['step'] )  ? absint( $_GET['step'] )  : 1;
			$total = isset( $_GET['total'] ) ? absint( $_GET['total'] ) : false;

			// Set Tables
			$stripe_table = $wpdb->prefix . 'nb_stripe_transactions';
			$entries_table = $wpdb->prefix . 'nb_stripe_entries';

			// Count rows
			$rows = $wpdb->get_col( $wpdb->prepare( "SELECT * FROM $stripe_table WHERE year(date) = %d AND month(date) = %d", $year, $month ) );
			$row_count = $wpdb->num_rows;

			// Set Total
			if ( empty( $total ) || $total <= 1 ) {
				$total = $wpdb->num_rows;
			}

			// Set Offest
			$offset = round( ( $step * 50 - 50 ), 0 );

			// Get all transactions
			$transactions = $wpdb->get_results( $wpdb->prepare( "SELECT * FROM $stripe_table
				WHERE year(date) = %d AND month(date) = %d ORDER BY date ASC LIMIT %d, %d", $year, $month, $offset, 50 ) );

			if ( $transactions ) {

				foreach ( $transactions as $transaction ) {

					// Exclude certain transactions
					$exclude = self::get_excluded_types();

					if ( ! in_array( $transaction->type, $exclude ) ) {

						// Variables
						$invoice_id = false;
						$customer_id = false;
						$gegenkonto = false;
						$kurs = false;

						// Set Konto and Kennzeichen
						$konto = 1373;
						$kennzeichen = ( $transaction->total > 0 ) ? 'S' : 'H';

						// Get Invoice
						$invoice = self::get_invoice_by_payment_id( $transaction->payment_id, $transaction->type );

						// Set Invoice Data
						if ( $invoice ) {

							$invoice_id = esc_attr( $invoice->invoice_prefix . $invoice->invoice_id );
							$customer_id = (int) $invoice->customer_id;
							$gegenkonto = (int) $invoice->invoice_debitor;

							// Set Konto to 1490 for IST-Versteuerung
							if ( 4400 == $invoice->invoice_account ) {
								$konto = 1490;
							}
						}

						$data = array(
							'id'          => esc_attr( $transaction->id ),
							'date'        => esc_attr( date( 'Y-m-d', strtotime( $transaction->date ) ) ),
							'time'        => esc_attr( date( 'H:i', strtotime( $transaction->date ) ) ),
							'type'        => esc_attr( $transaction->type ),
							'description' => esc_attr( $transaction->description ),
							'email'       => esc_attr( $transaction->email ),
							'payment_id'  => esc_attr( $transaction->payment_id ),
							'total'       => floatval( round( $transaction->total, 2 ) ),
							'currency'    => esc_attr( $transaction->currency ),
							'invoice_id'  => $invoice_id,
							'customer_id' => $customer_id,
							'konto'       => $konto,
							'gegenkonto'  => $gegenkonto,
							'kennzeichen' => $kennzeichen,
							'kurs'        => $kurs,
						);

						// Insert on duplicate
						self::insert_on_duplicate( $entries_table, $data );

					} // End if().
				} // End foreach().

				// Redirect
				$step++;
				$redirect = add_query_arg( array(
					'page'  => 'netzberufler-stripe',
					'tab'   => 'process',
					'year'  => $year,
					'month' => $month,
					'step'  => $step,
					'total' => $total,
				), admin_url( 'admin.php' ) );
				wp_redirect( $redirect );
				exit;

			} else {

				// Process Stripe Fees
				self::process_stripe_fees( $year, $month );

				// No more comments found, finish up
				wp_redirect( admin_url( 'admin.php?page=netzberufler-stripe' ) );
				exit;

			} // End if().
		}

		/**
		 * Process Stripe Fees
		 *
		 * @param int $invoice_id
		 */
		static function process_stripe_fees( $year = 0, $month = 0 ) {
			global $wpdb;

			// Set Variables
			$fees = 0;

			// Set Tables
			$stripe_table = $wpdb->prefix . 'nb_stripe_transactions';
			$entries_table = $wpdb->prefix . 'nb_stripe_entries';

			// Get all transactions for month
			$transactions = $wpdb->get_results( $wpdb->prepare(
				"SELECT * FROM $stripe_table
				WHERE YEAR(date) = %d AND MONTH(date) = %d
				ORDER BY date DESC", $year, $month ) );

			// Loop transactions
			foreach ( $transactions as $transaction ) :

				// Exclude certain transactions
				$exclude = self::get_excluded_types();

				if ( ! in_array( $transaction->type, $exclude ) ) {

					$fees += $transaction->fee;

				}

			endforeach;

			// Fees are negative.
			$fees = $fees * -1;

			// Set Kennzeichen
			$kennzeichen = ( $fees > 0 ) ? 'S' : 'H';

			// Set Transaction ID
			$payment_id = 'stripe_fees_' . date( 'M', mktime( 0, 0, 0, $month, 1, $year ) ) . '_' . $year;

			// Enter Fees
			if ( 0 !== $fees ) {

				$data = array(
					'id'          => $payment_id,
					'date'        => date( 'Y-m-t', mktime( 0, 0, 0, $month, 1, $year ) ),
					'time'        => '23:59:59',
					'type'        => 'fees',
					'description' => 'Stripe Zahlungsgebühren',
					'email'       => '',
					'payment_id'  => $payment_id,
					'total'       => floatval( round( $fees, 2 ) ),
					'currency'    => 'EUR',
					'invoice_id'  => false,
					'customer_id' => false,
					'konto'       => 1373,
					'gegenkonto'  => 6855,
					'kennzeichen' => $kennzeichen,
				);

				// Insert on duplicate
				self::insert_on_duplicate( $entries_table, $data );
			}
		}

		/**
		 * Get Invoice Object by Stripe Transaction ID
		 *
		 * @param int $invoice_id
		 * @return object $invoice Invoice Object
		*/
		static function get_invoice_by_payment_id( $payment_id = '', $type = '' ) {
			global $wpdb;

			// Sanitize Transaction ID
			$payment_id = sanitize_text_field( $payment_id );

			// Return early when arguments are missing
			if ( '' === $payment_id or '' === $type ) {
				return false;
			}

			// Set Invoice Table
			$invoice_table = $wpdb->prefix . 'nb_invoices';

			// Check if it is a refund.
			if ( 'refund' === $type ) {

				// Get Reversal Invoice from Database
				$invoice = $wpdb->get_row( $wpdb->prepare( "SELECT * FROM $invoice_table
					WHERE invoice_status = 'reversal' AND stripe_payment_id = %s", $payment_id ) );

			} else {

				// Get Invoice from Database
				$invoice = $wpdb->get_row( $wpdb->prepare( "SELECT * FROM $invoice_table
					WHERE invoice_status <> 'reversal' AND stripe_payment_id = %s", $payment_id ) );

			}

			return $invoice;
		}

		static function insert_on_duplicate( $table, $data ) {
			global $wpdb;

			// Get Fields
			$fields = array_keys( $data );
			$formatted_fields = array();

			foreach ( $fields as $field ) {
				$form = '%s';
				$formatted_fields[] = $form;
			}

			$sql = "INSERT INTO `$table` (`" . implode( '`,`', $fields ) . "`) VALUES ('" . implode( "','", $formatted_fields ) . "')";
			$sql .= ' ON DUPLICATE KEY UPDATE ';

			$dup = array();
			foreach ( $fields as $field ) {
				$dup[] = '`' . $field . '` = VALUES(`' . $field . '`)';
			}

			$sql .= implode( ',', $dup );

			return $wpdb->query( $wpdb->prepare( $sql, $data ) );
		}

		static function get_excluded_types() {

			// Exclude certain transactions
			$exclude = array();

			return $exclude;
		}
	}

	add_action( 'admin_init', array( 'NB_Stripe_Process', 'process_batch' ) );

endif;
