<?php

/**
 * Return Price with Currency Symbol
 *
 * @return string Formatted Price
*/
function nb_stripe_format_price( $price = 0, $currency ) {

	// Format Price
	$price = floatval( round( $price, 2 ) );
	$price = number_format( $price, 2, ',', '.' );

	// Add currency
	if ( 'EUR' === $currency ) {

		return $price . '€';

	} else {

		return '$' . $price;

	}
}
