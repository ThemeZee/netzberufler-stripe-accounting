<?php

/**
 * Display Update Notice
 *
 * @return string Update Notice
*/
function nb_stripe_display_update_notice( $notice = '' ) {
	?>

	<div class="updated settings-error notice is-dismissible">
		<p><strong><?php echo $notice; ?></strong></p>
		<button class="notice-dismiss" type="button"><span class="screen-reader-text"><?php esc_html_e( 'Dismiss this notice.', 'netzberufler-stripe' ); ?></span></button>
	</div>
	
	<?php
}

/**
 * Display Error Notice
 *
 * @return string Error Notice
*/
function nb_stripe_display_error_notice( $notice = '' ) {
	?>

	<div class="error settings-error notice">
		<p><strong><?php echo $notice; ?></strong></p>
	</div>
	
	<?php
}
