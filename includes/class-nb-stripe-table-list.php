<?php
/***
 * Invoice List
 *
 * Displays all invoices in a table
 *
 * @package Netzberufler Stripe Accounting
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) { exit;
}


// Use class to avoid namespace collisions
if ( ! class_exists( 'NB_Stripe_Table_List' ) ) :

	class NB_Stripe_Table_List {
		/** Singleton *************************************************************/

		/**
		 * @var Pagination
		 */
		protected $current_page;
		protected $items_per_page = 50;
		protected $total_items;
		protected $total_pages;
		protected $offset;

		function get_filter_fields( $table, $field ) {
			global $wpdb;

			$query = $wpdb->get_results( "SELECT $field FROM $table GROUP by $field ORDER BY $field DESC" );

			return $query;
		}

		static function get_monthly_dates( $table, $field ) {
			global $wpdb;

			$query = $wpdb->get_results( "SELECT YEAR($field) as 'year', MONTH($field) as 'month'
						FROM $table GROUP by YEAR($field), MONTH($field) ORDER BY YEAR($field) DESC, MONTH($field) DESC");

			return $query;
		}

		function monthly_dates_select( $dates, $current_date ) {
			?>

				<select id="date" name="date">

				<?php
				echo '<option value="" ' . selected( '', $current_date, false ) . '>' . esc_html__( 'All Dates', 'themezee-invoices' ) . '</option>';

				foreach ( $dates as $date ) :

					$key = $date->year . $date->month;
					$value = date( 'F Y', mktime( 0, 0, 0, $date->month, 1, $date->year ) );

					echo '<option value="' . esc_attr( $key ) . '"' . selected( $key, $current_date, false ) . '>' . esc_html( $value ) . '</option>';

					endforeach;
				?>

				</select>

			<?php
		}

		function setup_pagination( $db_query ) {
			global $wpdb;

			// Set Current Page
			$this->current_page = ( isset( $_GET['paged'] ) and $_GET['paged'] > 0 ) ? (int) $_GET['paged'] : 1;

			// Count rows
			$rows = $wpdb->get_col( $db_query );
			$total_items = $wpdb->num_rows;

			// Setup Variables
			$this->total_items = $total_items;
			$this->total_pages = ceil( $this->total_items / $this->items_per_page );
			$this->offset = $this->pagination_offset( $this->total_pages, $this->current_page, $this->items_per_page );
		}

		function pagination_total_items( $db_query ) {
			global $wpdb;

			// Count rows
			$rows = $wpdb->get_col( $db_query );
			$total_items = $wpdb->num_rows;

			// Return Row Count
			return (int) $total_items;
		}

		function pagination_offset( $total_pages, $current, $items_per_page ) {

			// Set Current Page
			if ( $items_per_page < 1 ) {

				$current = 1;

			} elseif ( $current > $total_pages ) {

				$current = $total_pages;

			}

			// Set Offest
			$offset = ( $current - 1 ) * $items_per_page;

			// Make sure offest is not negative
			$offset = ( $offset > 0 ) ? (int) $offset : 0;

			return $offset;

		}

		/**
		 * Display the pagination.
		 */
		protected function pagination( $total_items, $total_pages, $current ) {

			$output = '<span class="displaying-num">' . sprintf( _n( '%s item', '%s items', $total_items ), number_format_i18n( $total_items ) ) . '</span>';

			$current_url = set_url_scheme( 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] );
			$page_links = array();

			$total_pages_before = '<span class="paging-input">';
			$total_pages_after  = '</span>';

			$disable_first = $disable_last = $disable_prev = $disable_next = false;

			if ( $current == 1 ) {
				$disable_first = true;
				$disable_prev = true;
			}
			if ( $current == 2 ) {
				$disable_first = true;
			}
			if ( $current == $total_pages ) {
				$disable_last = true;
				$disable_next = true;
			}
			if ( $current == $total_pages - 1 ) {
				$disable_last = true;
			}

			if ( $disable_first ) {
				$page_links[] = '<span class="tablenav-pages-navspan" aria-hidden="true">&laquo;</span>';
			} else {
				$page_links[] = sprintf( "<a class='first-page' href='%s'><span class='screen-reader-text'>%s</span><span aria-hidden='true'>%s</span></a>",
					esc_url( remove_query_arg( 'paged', $current_url ) ),
					__( 'First page' ),
					'&laquo;'
				);
			}

			if ( $disable_prev ) {
				$page_links[] = '<span class="tablenav-pages-navspan" aria-hidden="true">&lsaquo;</span>';
			} else {
				$page_links[] = sprintf( "<a class='prev-page' href='%s'><span class='screen-reader-text'>%s</span><span aria-hidden='true'>%s</span></a>",
					esc_url( add_query_arg( 'paged', max( 1, $current -1 ), $current_url ) ),
					__( 'Previous page' ),
					'&lsaquo;'
				);
			}

			$html_current_page = sprintf( "%s<input class='current-page' id='current-page-selector' type='text' name='paged' value='%s' size='%d' aria-describedby='table-paging' />",
				'<label for="current-page-selector" class="screen-reader-text">' . __( 'Current Page' ) . '</label>',
				$current,
				strlen( $total_pages )
			);

			$html_total_pages = sprintf( "<span class='total-pages'>%s</span>", number_format_i18n( $total_pages ) );
			$page_links[] = $total_pages_before . sprintf( _x( '%1$s of %2$s', 'paging' ), $html_current_page, $html_total_pages ) . $total_pages_after;

			if ( $disable_next ) {
				$page_links[] = '<span class="tablenav-pages-navspan" aria-hidden="true">&rsaquo;</span>';
			} else {
				$page_links[] = sprintf( "<a class='next-page' href='%s'><span class='screen-reader-text'>%s</span><span aria-hidden='true'>%s</span></a>",
					esc_url( add_query_arg( 'paged', min( $total_pages, $current + 1 ), $current_url ) ),
					__( 'Next page' ),
					'&rsaquo;'
				);
			}

			if ( $disable_last ) {
				$page_links[] = '<span class="tablenav-pages-navspan" aria-hidden="true">&raquo;</span>';
			} else {
				$page_links[] = sprintf( "<a class='last-page' href='%s'><span class='screen-reader-text'>%s</span><span aria-hidden='true'>%s</span></a>",
					esc_url( add_query_arg( 'paged', $total_pages, $current_url ) ),
					__( 'Last page' ),
					'&raquo;'
				);
			}

			$pagination_links_class = 'pagination-links';
			if ( ! empty( $infinite_scroll ) ) {
				$pagination_links_class = ' hide-if-js';
			}
			$output .= "\n<span class='$pagination_links_class'>" . join( "\n", $page_links ) . '</span>';

			if ( $total_pages ) {
				$page_class = $total_pages < 2 ? ' one-page' : '';
			} else {
				$page_class = ' no-pages';
			}

			$pagination = "<div class='tablenav-pages{$page_class}'>$output</div>";

			echo $pagination;
		}
	}

endif;
