<?php
/***
 * Invoice Admin Page Class
 *
 * Adds a new admin page in the WordPress Backend
 *
 * @package Netzberufler Stripe Accounting
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// Use class to avoid namespace collisions
if ( ! class_exists( 'Netzberufler_Stripe_Admin_Page' ) ) :

	class Netzberufler_Stripe_Admin_Page {

		/**
		 * Setup the Admin Page class
		 *
		 * @return void
		*/
		static function setup() {

			// Add Invoice Admin Page
			add_action( 'admin_menu', array( __CLASS__, 'add_admin_page' ) );

			// Enqueue Admin Page CSS Stylesheet
			add_action( 'admin_enqueue_scripts', array( __CLASS__, 'enqueue_styles' ) );
		}

		/**
		 * Add admin page in WordPress backend
		 *
		 * @return void
		*/
		static function add_admin_page() {

			add_menu_page( 'NB Stripe', 'NB Stripe', 'manage_options', 'netzberufler-stripe', array( __CLASS__, 'display_admin_page' ), '', 122 );

		}

		/**
		 * Display admin page
		 *
		 * @return void
		*/
		static function display_admin_page() {

			ob_start();
			?>

			<div id="nb-stripe-admin-page" class="nb-stripe-admin-page-wrap wrap">

				<h2 id="nb-stripe-admin-page-navigation" class="nb-stripe-admin-page-nav nav-tab-wrapper nb-stripe-clearfix">

					<?php self::display_tabs_navigation(); ?>

				</h2>

				<div id="nb-stripe-admin-page-tab-content" class="nb-stripe-admin-page-content nb-stripe-clearfix">

					<?php self::display_tabs_content(); ?>

				</div>

			</div>

			<?php
			echo ob_get_clean();
		}

		/**
		 * Display tabs navigation on admin page
		 *
		 * @return String $links List of Tabs
		 */
		static function display_tabs_navigation( $current = 'entries' ) {

			// Get the current tab
			$current = isset( $_GET['tab'] ) ? esc_attr( $_GET['tab'] ) : 'entries';

			// Tabs
			$tabs = array(
			'entries' => esc_html__( 'Entries', 'netzberufler-stripe' ),
			'transactions' => esc_html__( 'Transactions', 'netzberufler-stripe' ),
			'statistics' => esc_html__( 'Statistics', 'netzberufler-stripe' ),
			'process-page' => esc_html__( 'Process', 'netzberufler-stripe' ),
			'import' => esc_html__( 'Import', 'netzberufler-stripe' ),
			'export' => esc_html__( 'Export', 'netzberufler-stripe' ),
			);

			// Loop to create Tabs Navigation
			$links = array();
			foreach ( $tabs as $tab => $name ) :
				if ( $tab == $current ) :
					$links[] = "<a class=\"nav-tab nav-tab-active\" href=\"?page=netzberufler-stripe&tab=$tab\">$name</a>";
				else :
					$links[] = "<a class=\"nav-tab\" href=\"?page=netzberufler-stripe&tab=$tab\">$name</a>";
				endif;
			endforeach;

			// Display Tab Navigaiton
			foreach ( $links as $link ) {

				echo $link;

			}
		}

		/**
		 * Display tabs content on admin page
		 *
		 * @return void
		 */
		static function display_tabs_content( $current = 'invoices' ) {

			// Get the current tab
			$tab = isset( $_GET['tab'] ) ? esc_attr( $_GET['tab'] ) : 'entries';

			// Output Tabs Content
			switch ( $tab ) {
				case 'entries' : NB_Stripe_Entries::instance()->entries_table(); break;
				case 'transactions' : NB_Stripe_Transactions::instance()->transactions_table(); break;
				case 'statistics' : NB_Stripe_Statistics::instance()->statistics_table(); break;
				case 'process' : NB_Stripe_Process::process(); break;
				case 'process-page' : NB_Stripe_Process_Page::process_form(); break;
				case 'import' : NB_Stripe_Import::import_form(); break;
				case 'export' : NB_Stripe_Export::export(); break;
				default: NB_Stripe_Entries::instance()->entries_table(); break;
			}
		}

		/**
		 * Enqueue Admin Page Stylesheet
		 *
		 * @return void
		 */
		static function enqueue_styles( $hook ) {

			// Load styles and scripts only on theme info page
			if ( 'toplevel_page_netzberufler-stripe' != $hook ) {
				return;
			}

			// Enqueue Plugin Stylesheet
			wp_enqueue_style( 'netzberufler-stripe', NB_STRIPE_PLUGIN_URL . 'assets/css/netzberufler-stripe.css', array(), NB_STRIPE_VERSION );

			// Enqueue CSV Upload JS
			wp_enqueue_script( 'netzberufler-stripe-csv-upload', NB_STRIPE_PLUGIN_URL . 'assets/js/csv-upload.js', array( 'jquery' ), NB_STRIPE_VERSION );
			wp_enqueue_media();

		}
	}

	// Run TZ Invoice Admin Page Class
	Netzberufler_Stripe_Admin_Page::setup();

endif;
